﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ToolBoxMVVM.Mediator
{
    public class Messenger
    {
        private static Messenger _instance;
        public static Messenger Instance 
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new Messenger();
                }
                return _instance;
            }
        }

        private Messenger()
        {
            _events = new Dictionary<string, Action>();
        }

        private Dictionary<string, Action> _events;

        public void Subscribe(string topic, Action action)
        {
            if(_events.ContainsKey(topic))
            {
                _events[topic] += action;
            }
            else
            {
                _events.Add(topic, action);
            }
        }

        public void publish(string topic)
        {
            if(_events.ContainsKey(topic))
            {
                _events[topic]?.Invoke();
            }
        }
    }public class Messenger<T>
    {
        private static Messenger<T> _instance;
        public static Messenger<T> Instance 
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new Messenger<T>();
                }
                return _instance;
            }
        }

        private Messenger()
        {
            _events = new Dictionary<string, Action<T>>();
        }

        private Dictionary<string, Action<T>> _events;

        public void Subscribe(string topic, Action<T> action)
        {
            if(_events.ContainsKey(topic))
            {
                _events[topic] += action;
            }
            else
            {
                _events.Add(topic, action);
            }
        }

        public void publish(string topic, T parameter)
        {
            if(_events.ContainsKey(topic))
            {
                _events[topic]?.Invoke(parameter);
            }
        }
    }
}
