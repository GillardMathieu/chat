﻿using Chat.Client.Models;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxMVVM.BaseClasses;

namespace Chat.Client.ViewModels
{
    class MainViewModel : BindableBase
    {
        private string _content;

        public string Content
        {
            get { return _content; }
            set { _content = value; RaisePropertyChanged(); }
        }

        private ObservableCollection<Message> _messages;

        public ObservableCollection<Message> Messages
        {
            get { return _messages; }
            set { _messages = value; RaisePropertyChanged(); }
        }

        private string _token;

        public string Token
        {
            get { return _token; }
            set { _token = value; }
        }

        private string _username;

        public string UserName
        {
            get { return _username; }
            set { _username = value; RaisePropertyChanged(); }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; RaisePropertyChanged(); }
        }

        public RelayCommand SendCmd
        { get
            {
                return new RelayCommand(() =>
                {
                    if (Token == null) return;
                    _hub.Invoke("SendToAll", new Message { Author = "Mathieu", Date = DateTime.Now, Content = Content, Token = Token });
                    Content = null;
                });
            }
        }

        public RelayCommand SignInCmd
        {
            get
            {
                return new RelayCommand(() =>
                {
                    _hub.Invoke("Signin", new Login { Username = UserName, Password = Password });
                });
            }
        }

        private HubConnection _conn;
        private IHubProxy _hub;

        public MainViewModel()
        {
            Messages = new ObservableCollection<Message>();
            _conn = new HubConnection("http://Khun.somee.com/signalr");
            _hub = _conn.CreateHubProxy("MessageHub");
            _conn.Start().Wait();
            _hub.On<string>("NewToken", (token) => {
                App.Current.Dispatcher.Invoke(() =>
                {
                    Token = token;
                });
            });
            _hub.On<Message>("NewMessage", (m) => {
                App.Current.Dispatcher.Invoke(() =>
                {
                    Messages.Add(m);
                });
            });
            _hub.On<string>("Error", (error) => { 

            });
        }


    }
}
